import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appColory]',
  exportAs: 'colory',
})
export class ColoryDirective {
  private counter: number = 0;

  @HostBinding('style.color') color: string;
  @HostListener('click', ['event']) changeColor(event) {
    this.setRandomColor();
    this.counter++;
    console.log(this.counter);
  }

  setRandomColor(): void {
    this.color = `#${Math.floor(Math.random() * 8897).toString(16)}`;
  }

  constructor() {
    this.color = 'red';
  }

  ngOnInit() {
    setTimeout(() => (this.color = 'green'), 1000);
  }
}
