import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, ReflectiveInjector } from '@angular/core';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CardComponent } from './card/card.component';
import { ColoryDirective } from './colory.directive';
import { DelayDirective } from './delay.directive';
import { UserService } from './user.service';
import { InterceptorService } from './interceptor.service';

const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');
// const injector: Injector = ReflectiveInjector.resolveAndCreate({
//   provide: UserService,
//   useClass: UserService,
// });
// const userService = injector.get(UserService);
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CardComponent,
    ColoryDirective,
    DelayDirective,
  ],
  entryComponents: [FooterComponent],
  imports: [BrowserModule, HttpClientModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: UserService, useClass: UserService, deps: [HttpClient] },
    { provide: API_BASE_URL, useValue: 'api.mysite.com' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
