import { ViewContainerRef } from '@angular/core';
import { OnInit } from '@angular/core';
import { ComponentFactoryResolver } from '@angular/core';
// import { SimpleChanges } from '@angular/core';
import { Component } from '@angular/core';
import { FooterComponent } from './footer/footer.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public dynamicColor = 'black';
  public dataProps = {
    title: 'loading...',
  };
  public showText = true;
  public arrNumbers = ['раз', 'два', 'три'];

  constructor(
    private view: ViewContainerRef,
    private comopnentFactoryResolver: ComponentFactoryResolver
  ) {
    console.log('constructor');
  }
  // ngOnChanges(changes: SimpleChanges) {
  //   console.log('ngOnChanges ', changes);
  // }
  ngOnInit() {
    console.log('ngOnInit');
    setTimeout(() => {
      const componentFactory = this.comopnentFactoryResolver.resolveComponentFactory(
        FooterComponent
      );
      const componentRef = this.view.createComponent(componentFactory);
      console.log(componentRef);
      this.dataProps = {
        title: 'Boring appBar',
      };
    }, 1500);
  }
  // ngDoCheck() {
  //   console.log('ngDoCheck');
  // }
  // ngAfterContentInit() {
  //   console.log('ngAfterContentInit');
  // }
  // ngAfterContentChecked() {
  //   console.log('ngAfterContentChecked');
  // }
  // ngAfterViewInit() {
  //   console.log('ngAfterViewInit');
  // }
  // ngAfterViewChecked() {
  //   console.log('ngAfterViewChecked');
  // }
  // ngOnDestroy() {
  //   console.log('ngOnDestroy');
  // }

  changeColor(color) {
    console.log(color);
    this.dynamicColor = color;
  }

  toggleShowText() {
    return (this.showText = !this.showText);
  }

  cardClickedHandler(item) {
    console.log(`card ${item} clicked!`);
  }
}
