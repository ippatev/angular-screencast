import { HttpClient } from '@angular/common/http';

export class UserService {
  private users = [
    { name: 'Misha' },
    { name: 'Sasha' },
    { name: 'Pasha' },
    { name: 'Dasha' },
  ];
  constructor(private _httpClient: HttpClient) {}

  public getAll() {
    return this._httpClient.get('https://jsonplaceholder.typicode.com/users');
  }

  public remove(name: string) {
    return (this.users = this.users.filter((user) => user.name !== name));
  }

  public add(name: string) {
    return this.users.push({ name });
  }
}
