import {
  Directive,
  TemplateRef,
  OnInit,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[appDelay]',
})
export class DelayDirective implements OnInit {
  constructor(
    private tempalte: TemplateRef<any>,
    private view: ViewContainerRef
  ) {}
  ngOnInit(): void {
    setTimeout(() => this.view.createEmbeddedView(this.tempalte), 500);
  }
}
