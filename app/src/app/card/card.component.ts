import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  constructor() {}

  @Input() dataProps;
  @Output() cardClicked: EventEmitter<any> = new EventEmitter();
  ngOnInit(): void {}

  clickHandler() {
    this.cardClicked.emit(this.dataProps);
  }
}
