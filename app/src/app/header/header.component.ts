import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Input('dataProps') dataProps;
  public users;
  // private _userService;

  constructor(private _userService: UserService) {}

  ngOnInit(): void {
    this.updateUsers();
  }

  updateUsers() {
    this.users = this._userService.getAll().subscribe((users) => {
      this.users = users;
    });
  }

  removeUser(name: string) {
    this._userService.remove(name);
    this.updateUsers();
  }

  addUser(name: string) {
    this._userService.add(name);
    this.updateUsers();
  }
}
